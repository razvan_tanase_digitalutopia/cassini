//
//  DemoUrl.swift
//  Cassini
//
//  Created by Razvan Tanase on 29/08/2019.
//  Copyright © 2019 Razvan Tanase. All rights reserved.
//

import Foundation

internal struct DemoURLs{
    internal static let stanford = URL(string: "https://upload.wikimedia.org/wikipedia/commons/5/55/Stanford_Oval_September_2013_panorama.jpg")
    internal static var NASA : Dictionary<String,URL> = {
        let NASAURLStrings = ["Cassini" : "https://www.jpl.nasa.gov/images/cassini/2090202/pia03883-full.jpg",
                              "Earth" : "https://www.nasa.gov/sites/default/files/wave_earth_mosaic_3.jpg",
                              "Saturn" : "https://www.nasa.gov/sites/default/files/saturn_collage.jpg"]
        var urls = Dictionary<String, URL>()
        for(key,value) in NASAURLStrings{
            urls[key] = URL(string: value)
        }
        return urls
    }()
}
