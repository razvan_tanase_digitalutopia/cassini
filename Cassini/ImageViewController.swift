//
//  ImageViewController.swift
//  Cassini
//
//  Created by Razvan Tanase on 29/08/2019.
//  Copyright © 2019 Razvan Tanase. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController,UIScrollViewDelegate {

    var imageURL: URL?
    {
        didSet{
            image = nil
            if view.window != nil{
            fetchImage()
            }
        }
    }
    
    private var image: UIImage?{
        get{
            return ImageView.image
        }
        set{
            ImageView.image = newValue
            ImageView.sizeToFit()
            ScrollImageView?.contentSize = ImageView.frame.size
            spinner?.stopAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if ImageView.image == nil{
            fetchImage()
        }
    }
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    @IBOutlet weak var ScrollImageView: UIScrollView!{
        didSet{
            ScrollImageView.minimumZoomScale=1/25
            ScrollImageView.maximumZoomScale=1.0
            ScrollImageView.delegate = self
            ScrollImageView.addSubview(ImageView)
            
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return ImageView
    }
    
    var ImageView = UIImageView()
    
    private func fetchImage(){
        if let url = imageURL{
           spinner.startAnimating()
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                let urlContents = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if let imageData = urlContents, url == self?.imageURL{
                        self?.image = UIImage(data: imageData)
                    }
                }
                
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
